require 'test_helper'

class SessionPackagesControllerTest < ActionController::TestCase
  setup do
    @session_package = session_packages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:session_packages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create session_package" do
    assert_difference('SessionPackage.count') do
      post :create, session_package: { session_package_type: @session_package.session_package_type, speciality_id: @session_package.speciality_id, start_at: @session_package.start_at, user_id: @session_package.user_id }
    end

    assert_redirected_to session_package_path(assigns(:session_package))
  end

  test "should show session_package" do
    get :show, id: @session_package
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @session_package
    assert_response :success
  end

  test "should update session_package" do
    patch :update, id: @session_package, session_package: { session_package_type: @session_package.session_package_type, speciality_id: @session_package.speciality_id, start_at: @session_package.start_at, user_id: @session_package.user_id }
    assert_redirected_to session_package_path(assigns(:session_package))
  end

  test "should destroy session_package" do
    assert_difference('SessionPackage.count', -1) do
      delete :destroy, id: @session_package
    end

    assert_redirected_to session_packages_path
  end
end
