class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :user_type, index: true, foreign_key: true
    add_column :users, :phone, :string
    add_column :users, :name, :string
  end
end
