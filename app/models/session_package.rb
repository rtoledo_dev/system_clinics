class SessionPackage < ActiveRecord::Base
  belongs_to :user
  belongs_to :speciality
  has_many :user_session_packages
  has_many :pathologies
end
