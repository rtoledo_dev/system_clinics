json.array!(@session_packages) do |session_package|
  json.extract! session_package, :id, :start_at, :session_package_type, :user_id, :speciality_id
  json.url session_package_url(session_package, format: :json)
end
